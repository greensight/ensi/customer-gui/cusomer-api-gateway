OrderReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: ID заказа
      example: 1
    number:
      type: string
      description: номер заказа
      example: "1000001"
    client_comment:
      type: string
      description: Комментарий клиента к заказу
      example: 'Яиц ровно 5'
      nullable: true
    delivery_method:
      type: integer
      description: метод доставки
      example: 1
    delivery_price:
      type: integer
      description: Стоимость доставки
      nullable: true
      example: 10000
    delivery_address:
      $ref: './data/address.yaml#/Address'
    payment_method:
      allOf:
        - type: integer
        - $ref: '../enums/payment_method_enum.yaml'
    payment_system:
      allOf:
        - type: integer
        - $ref: '../enums/payment_system_enum.yaml'
    payment_status:
      allOf:
        - type: integer
        - $ref: '../enums/payment_status_enum.yaml'
    payment_url:
      type: string
      description: Ссылка для оплаты во внещней системе
      nullable: true
    cost:
      type: integer
      description: стоимость (без учета скидки) (рассчитывается автоматически) в коп.
      example: 10000
    price:
      type: integer
      description: стоимость (с учетом скидок) (рассчитывается автоматически) в коп.
      example: 10000
    qty:
      type: number
      description: Кол-во товара
      example: 1
    promo_code:
      type: string
      description: Примененный промокод
      nullable: true
      example: TEST
  required:
    - id
    - number
    - client_comment
    - delivery_method
    - delivery_address
    - payment_method
    - payment_system
    - cost
    - price
    - qty
    - promo_code


OrderIncludes:
  type: object
  properties:
    items:
      type: array
      items:
        $ref: './order_items.yaml#/OrderItem'
Order:
  allOf:
    - $ref: '#/OrderReadonlyProperties'
    - $ref: '#/OrderIncludes'
OrderResponse:
  type: object
  properties:
    data:
      $ref: '#/Order'
    meta:
      type: object
  required:
    - data


CustomerOrderReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: ID заказа
      example: 1
    number:
      type: string
      description: номер заказа
      example: "1000001"
    price:
      type: integer
      description: стоимость (с учетом скидок) (рассчитывается автоматически) в коп.
      example: 10000
    status_at:
      type: string
      format: date-time
      description: дата установки статуса заказа
      example: "2021-06-11T11:27:10.000000Z"
    payment_status:
      allOf:
        - type: integer
        - $ref: '../enums/payment_status_enum.yaml'
    payment_method:
      allOf:
        - type: integer
        - $ref: '../enums/payment_method_enum.yaml'
    payment_system:
      allOf:
        - type: integer
        - $ref: '../enums/payment_system_enum.yaml'
    payment_link:
      type: string
      description: Ссылка для оплаты во внешней системе
      nullable: true
    promo_code:
      type: string
      description: Примененный промокод
      nullable: true
      example: TEST
  required:
    - id
    - number
    - price
    - status_at
    - payment_status
    - payment_method
    - payment_system
    - promo_code

CustomerOrderFillableProperties:
  type: object
  properties:
    status:
      type: integer
      description: статус заказа из OrderStatus
      example: 1
    client_comment:
      type: string
      description: комментарий клиента
      example: "какой-то комментарий"
      nullable: true
  required:
    - status
    - client_comment

CustomerOrderDeliveryFillableProperties:
  type: object
  properties:
    delivery_method:
      type: integer
      description: метод доставки
      example: 1
    delivery_price:
      type: integer
      description: стоимость доставки (с учетом скидки) в копейках
      example: 20000
    delivery_comment:
      type: string
      description: комментарий к доставке
      example: "Остановиться перед воротами"
      nullable: true
    delivery_address:
      $ref: './data/address.yaml#/Address'
  required:
    - delivery_method
    - delivery_price
    - delivery_comment
    - delivery_address

CustomerOrderOneItemProperties:
  type: object
  properties:
    delivery_address:
      $ref: './data/address.yaml#/Address'
    order_items:
      type: array
      items:
        $ref: './order_items.yaml#/CustomerOrderItem'

CustomerOrderIncludes:
  type: object
  properties:
    items:
      type: array
      items:
        $ref: './order_items.yaml#/OrderItem'

CustomerOrderOneItem:
  allOf:
    - $ref: '#/CustomerOrderReadonlyProperties'
    - $ref: '#/CustomerOrderFillableProperties'
    - $ref: '#/CustomerOrderDeliveryFillableProperties'
    - $ref: '#/CustomerOrderOneItemProperties'

CustomerOrderResponse:
  type: object
  properties:
    data:
      $ref: '#/CustomerOrderOneItem'
    meta:
      type: object
  required:
    - data

CustomerOrderListItem:
  allOf:
    - $ref: '#/CustomerOrderReadonlyProperties'
    - $ref: '#/CustomerOrderFillableProperties'
    - $ref: '#/CustomerOrderDeliveryFillableProperties'

CustomerOrdersListRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

CustomerOrdersListResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/CustomerOrderListItem'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta
