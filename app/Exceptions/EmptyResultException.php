<?php

namespace App\Exceptions;

use Exception;

class EmptyResultException extends Exception
{
}
