<?php

namespace App\Http\ApiV1\Modules\Crm\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateCustomerFavoritesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'offer_ids' => ['required', 'array', 'min:1'],
            'offer_ids.*' => ['integer', 'distinct'],
        ];
    }

    public function getOfferIds(): array
    {
        return $this->input('offer_ids');
    }
}
