<?php

namespace App\Http\ApiV1\Modules\Crm\Resources;

use App\Domain\Crm\Data\FavoriteOfferData;
use App\Http\ApiV1\Modules\Catalog\Resources\Elastic\ElasticOffersResource;

/**
 * @mixin FavoriteOfferData
 */
class FavoritesResource extends ElasticOffersResource
{
    public function toArray($request): array
    {
        $data = parent::toArray($request);

        return array_merge($data, [
            'is_active' => $this->offer->getAllowPublish(),
        ]);
    }
}
