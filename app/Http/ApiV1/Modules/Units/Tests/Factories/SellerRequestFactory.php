<?php

namespace App\Http\ApiV1\Modules\Units\Tests\Factories;

use App\Domain\Common\Tests\Factories\BaseAddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SellerRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'legal_name' => $this->faker->unique()->company(),
            'legal_address' => BaseAddressFactory::new()->make(),
            'fact_address' => BaseAddressFactory::new()->make(),
            'inn' => $this->faker->numerify('##########'),
            'kpp' => $this->faker->numerify('#########'),
            'payment_account' => $this->faker->numerify('####################'),
            'correspondent_account' => $this->faker->numerify('####################'),
            'bank' => $this->faker->company(),
            'bank_address' => BaseAddressFactory::new()->make(),
            'bank_bik' => $this->faker->numerify('#########'),
            'site' => $this->faker->nullable()->url(),
            'info' => $this->faker->text(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
