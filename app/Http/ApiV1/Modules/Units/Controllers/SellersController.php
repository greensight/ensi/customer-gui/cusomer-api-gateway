<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\Bu\CreateSellerAction;
use App\Http\ApiV1\Modules\Units\Requests\CreateSellerRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class SellersController
{
    public function create(CreateSellerRequest $request, CreateSellerAction $action): Responsable
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
