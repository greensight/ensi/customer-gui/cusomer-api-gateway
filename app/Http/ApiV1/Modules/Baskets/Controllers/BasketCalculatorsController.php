<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Domain\Orders\Actions\Baskets\SetBasketPromoCodeAction;
use App\Http\ApiV1\Modules\Baskets\Queries\BasketCalculatorsQuery;
use App\Http\ApiV1\Modules\Baskets\Requests\SetBasketPromoCodeRequest;
use App\Http\ApiV1\Modules\Baskets\Resources\CalculateBasketsResource;
use App\Http\ApiV1\Modules\Baskets\Resources\PromoCodeBasketsResource;
use Illuminate\Contracts\Support\Responsable;

class BasketCalculatorsController
{
    public function current(BasketCalculatorsQuery $query): Responsable
    {
        return new CalculateBasketsResource($query->current());
    }

    public function promoCode(SetBasketPromoCodeRequest $request, SetBasketPromoCodeAction $action): Responsable
    {
        return new PromoCodeBasketsResource($action->execute($request->getPromoCode()));
    }
}
