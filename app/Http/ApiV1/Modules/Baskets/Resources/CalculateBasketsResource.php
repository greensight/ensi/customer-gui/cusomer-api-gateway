<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Orders\Data\Baskets\CalculateBasketData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CalculateBasketData */
class CalculateBasketsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'items' => CalculateBasketItemsResource::collection($this->getItems()),
            'promo_code' => $this->basket->getPromoCode(),
        ];
    }
}
