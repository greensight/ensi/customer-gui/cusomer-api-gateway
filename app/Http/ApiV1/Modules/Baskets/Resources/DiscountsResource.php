<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BasketsClient\Dto\Discount;

/** @mixin Discount */
class DiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'value' => $this->getValue(),
            'value_type' => $this->getValueType(),
        ];
    }
}
