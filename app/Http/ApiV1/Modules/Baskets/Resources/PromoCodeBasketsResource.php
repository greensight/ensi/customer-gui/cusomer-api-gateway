<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Orders\Data\Baskets\CalculateBasketData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

/** @mixin CalculateBasketData */
class PromoCodeBasketsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $isSuccess = $this->basket->getPromoCodeApplyStatus() == PromoCodeApplyStatusEnum::SUCCESS;

        return [
            'promo_code' => $this->basket->getPromoCode(),
            'success' => $isSuccess,
            'message' => !$isSuccess ? $this->basket->getPromoCodeApplyStatus() : null,
            'items' => $isSuccess ? CalculateBasketItemsResource::collection($this->getItems()) : null,
        ];
    }
}
