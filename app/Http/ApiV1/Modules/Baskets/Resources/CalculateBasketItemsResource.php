<?php

namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Orders\Data\Baskets\CalculateBasketItemData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductBrandsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductImagesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductNameplatesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CalculateBasketItemData */
class CalculateBasketItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->item->getOfferId(),
            'name' => $this->item->getName(),
            'code' => $this->item->getCode(),
            'barcode' => $this->item->getBarcode(),
            'vendor_code' => $this->item->getVendorCode(),
            'type' => $this->item->getType(),
            'is_adult' => $this->item->getIsAdult(),
            'main_image' => $this->item->getMainImage(),
            'qty' => $this->item->getQty(),
            'price_per_one' => $this->item->getPricePerOne(),
            'price' => $this->item->getPrice(),
            'cost_per_one' => $this->item->getCostPerOne(),
            'cost' => $this->item->getCost(),
            'is_favorite' => $this->isFavorite(),
            $this->mergeWhen($this->isWeight(), $this->getWeightOptions()),
            'discounts' => DiscountsResource::collection($this->whenNotNull($this->item->getDiscounts())),
            'images' => ProductImagesResource::collection($this->whenNotNull($this->item->getImages())),
            'nameplates' => ProductNameplatesResource::collection($this->whenNotNull($this->item->getNameplates())),
            'brand' => ProductBrandsResource::make($this->whenNotNull($this->item->getBrand())),
        ];
    }

    public function getWeightOptions(): array
    {
        return ['weight_options' => [
            'uom' => $this->item->getUom(),
            'tariffing_volume' => $this->item->getTariffingVolume(),
            'order_step' => $this->item->getOrderStep(),
            'order_minvol' => $this->item->getOrderMinvol(),
        ]];
    }
}
