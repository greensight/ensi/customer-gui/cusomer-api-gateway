<?php

use App\Http\ApiV1\Modules\Baskets\Tests\Factories\BasketItemsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/baskets:set-item 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockBasketsBasketApi()->shouldReceive('setBasketCustomerItem');

    $request = BasketItemsRequestFactory::new()->make();
    postJson('/api/v1/baskets:set-item', $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('DELETE /api/v1/baskets 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockBasketsBasketApi()->shouldReceive('deleteBasketCustomer');

    deleteJson('/api/v1/baskets')
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
