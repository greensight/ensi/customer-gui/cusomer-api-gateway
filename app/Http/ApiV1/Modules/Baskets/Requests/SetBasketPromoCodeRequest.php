<?php

namespace App\Http\ApiV1\Modules\Baskets\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SetBasketPromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'promo_code' => ['nullable', 'string'],
        ];
    }

    public function getPromoCode(): ?string
    {
        return $this->input('promo_code');
    }
}
