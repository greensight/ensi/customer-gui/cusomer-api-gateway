<?php

namespace App\Http\ApiV1\Modules\Baskets\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SetBasketItemsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'items' => ['required', 'array'],
            'items.*.offer_id' => ['required', 'integer'],
            'items.*.qty' => ['required', 'numeric', 'min:0'],
        ];
    }

    public function getItems(): array
    {
        return $this->input('items');
    }
}
