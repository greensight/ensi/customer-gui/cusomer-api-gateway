<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\MassSetNotificationViewedAction;
use App\Http\ApiV1\Modules\Communication\Queries\NotificationsQuery;
use App\Http\ApiV1\Modules\Communication\Requests\MassNotificationsViewedRequest;
use App\Http\ApiV1\Modules\Communication\Resources\NotificationsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class NotificationsController
{
    public function search(NotificationsQuery $query): Responsable
    {
        return NotificationsResource::collectPage($query->get());
    }

    public function massSetViewed(MassNotificationsViewedRequest $request, MassSetNotificationViewedAction $action): Responsable
    {
        $action->execute($request->getNotificationIds());

        return new EmptyResource();
    }
}
