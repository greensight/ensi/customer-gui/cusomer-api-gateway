<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassNotificationsViewedRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'notification_ids' => ['required', 'array', 'min:1'],
            'notification_ids.*' => ['integer'],
        ];
    }

    public function getNotificationIds(): array
    {
        return $this->input('notification_ids');
    }
}
