<?php

use App\Domain\Communication\Tests\Factories\NotificationFactory;
use App\Http\ApiV1\Modules\Communication\Tests\Factories\MassUpdateNotificationsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CommunicationManagerClient\Dto\EmptyDataResponse;
use Ensi\CommunicationManagerClient\Dto\MassPatchNotificationsRequest;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('communication', 'component');

test('POST /api/v1/customers/notifications:search 200', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $this->mockCommunicationNotificationsApi()->allows([
        'searchNotifications' => NotificationFactory::new()->makeResponseSearch(),
    ]);
    postJson('/api/v1/customers/notifications:search')
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'theme', 'text', 'is_viewed', 'created_at']]]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/customers/notifications:viewed 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $notificationId = 1;
    $notificationTwoId = 2;


    $this->mockCommunicationNotificationsApi()
        ->shouldReceive('massPatchNotifications')
        ->andReturn(new EmptyDataResponse())
        ->withArgs(function (MassPatchNotificationsRequest $requestInclude) use ($notificationId, $notificationTwoId) {
            $filter = $requestInclude->getFilter();

            return $filter->id === [$notificationId, $notificationTwoId];
        });

    $request = MassUpdateNotificationsRequestFactory::new()->make([
        'notification_ids' => [$notificationId, $notificationTwoId],
    ]);

    postJson('/api/v1/customers/notifications:viewed', $request)
        ->assertOk();
});
