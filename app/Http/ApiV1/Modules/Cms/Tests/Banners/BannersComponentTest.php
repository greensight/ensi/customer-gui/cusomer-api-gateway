<?php

use App\Domain\Cms\Tests\Factories\Pages\BannerButtonFactory;
use App\Domain\Cms\Tests\Factories\Pages\BannerFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms');

test('POST /api/v1/banners:search success', function () {
    $bannerId = 1;
    $slug = 'example-banner';

    $this->mockCmsBannersApi()->allows([
        'searchBanners' => BannerFactory::new()->makeResponseSearch([
            ['id' => $bannerId, 'code' => $slug],
        ]),
    ]);

    postJson('/api/v1/banners:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $bannerId)
        ->assertJsonPath('data.0.code', $slug);
});

test('POST /api/v1/banners:search with button success', function () {
    $bannerId = 1;
    $slug = 'example-banner';

    $button = BannerButtonFactory::new()->make();

    $this->mockCmsBannersApi()->allows([
        'searchBanners' => BannerFactory::new()
            ->withButton($button)
            ->makeResponseSearch([
                ['id' => $bannerId, 'code' => $slug],
            ]),
    ]);

    postJson('/api/v1/banners:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $bannerId)
        ->assertJsonPath('data.0.code', $slug)
        ->assertJsonPath('data.0.button.id', $button->getId());
});
