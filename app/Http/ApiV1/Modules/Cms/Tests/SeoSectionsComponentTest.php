<?php

use App\Domain\Cms\Tests\Factories\Seo\SeoTemplateFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/sections/products/{id} 200', function () {
    $productId = 1;
    $name = "Foo";

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCmsSeoTemplatesApi()
        ->shouldReceive('searchOneSeoTemplate')
        ->andReturn(SeoTemplateFactory::new()->makeResponse(['name' => $name]));

    getJson("/api/v1/sections/products/{$productId}")
        ->assertOk()
        ->assertJsonPath('data.name', $name)
        ->assertJsonStructure(['data' => ['id', 'name', 'header']]);
});
