<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\SeoSectionsQuery;
use App\Http\ApiV1\Modules\Cms\Resources\SeoSectionsResource;
use Illuminate\Contracts\Support\Responsable;

class SeoSectionsController
{
    public function getProductSection(int $id, SeoSectionsQuery $query): Responsable
    {
        return SeoSectionsResource::make($query->getByProductId($id));
    }
}
