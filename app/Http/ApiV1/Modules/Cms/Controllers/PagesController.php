<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\PagesQuery;
use App\Http\ApiV1\Modules\Cms\Resources\PagesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PagesController
{
    public function search(PagesQuery $query): AnonymousResourceCollection
    {
        return PagesResource::collectPage($query->get());
    }
}
