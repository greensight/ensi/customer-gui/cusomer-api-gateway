<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use Closure;
use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CmsClient\Dto\SearchOneSeoTemplateRequest;
use Ensi\CmsClient\Dto\SearchOneSeoTemplateRequestSeoVariables as SeoVariables;
use Ensi\CmsClient\Dto\SearchOneSeoTemplateRequestSeoVariablesPayload as SeoVariablesPayload;
use Ensi\CmsClient\Dto\SeoTemplate;

class SeoSectionsQuery
{
    public function __construct(protected readonly SeoTemplatesApi $api)
    {
    }

    public function getByProductId(int $productId): SeoTemplate
    {
        $request = new SearchOneSeoTemplateRequest();
        $request->setFilter((object)['template_product_id' => $productId]);

        $seoVariables = $this->makeSeoVariables(
            fn (SeoVariablesPayload $payload) => $payload->setProductId($productId),
        );

        $request->setSeoVariables($seoVariables);

        return $this->api->searchOneSeoTemplate($request)->getData();
    }

    protected function makeSeoVariables(Closure $fillPayload): SeoVariables
    {
        $payload = new SeoVariablesPayload();
        $fillPayload($payload);

        $seoVariables = new SeoVariables();
        $seoVariables->setFill(true);
        $seoVariables->setPayload($payload);

        return $seoVariables;
    }
}
