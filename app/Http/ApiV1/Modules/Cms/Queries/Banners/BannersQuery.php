<?php

namespace App\Http\ApiV1\Modules\Cms\Queries\Banners;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchBannersRequest;
use Ensi\CmsClient\Dto\SearchBannersResponse;
use Illuminate\Http\Request;

class BannersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        Request $httpRequest,
        protected BannersApi $bannersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchBannersRequest::class;
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBannersResponse
    {
        /** @var SearchBannersRequest $request */
        $request->setInclude(['button']);

        return $this->bannersApi->searchBanners($request);
    }
}
