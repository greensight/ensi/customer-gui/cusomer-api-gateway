<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\SeoTemplate;

/** @mixin SeoTemplate */
class SeoSectionsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'type' => $this->getType(),
            'header' => $this->getHeader(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'seo_text' => $this->getSeoText(),
        ];
    }
}
