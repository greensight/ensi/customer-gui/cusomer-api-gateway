<?php

namespace App\Http\ApiV1\Modules\Cms\Resources\Banners;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\Banner;

/**
 * @mixin Banner
 */
class BannersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'is_active' => $this->getIsActive(),
            'desktop_image' => $this->getDesktopImage()?->getUrl(),
            'mobile_image' => $this->getMobileImage()?->getUrl(),
            'url' => $this->getUrl(),

            'button' => BannerButtonsResource::make($this->whenNotNull($this->getButton())),
        ];
    }
}
