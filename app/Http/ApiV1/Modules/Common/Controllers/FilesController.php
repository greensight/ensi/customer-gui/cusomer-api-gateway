<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Actions\GetProtectedFilePathAction;
use App\Http\ApiV1\Modules\Common\Requests\DownloadFileProtectedRequest;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FilesController
{
    public function downloadProtected(DownloadFileProtectedRequest $request, GetProtectedFilePathAction $action): BinaryFileResponse
    {
        $path = $action->execute($request->all());

        if (!$path) {
            throw new NotFoundHttpException();
        }

        return response()->download($path);
    }
}
