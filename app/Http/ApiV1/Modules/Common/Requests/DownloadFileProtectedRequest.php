<?php

namespace App\Http\ApiV1\Modules\Common\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DownloadFileProtectedRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'entity' => ['required', 'string'],
            'entity_id' => ['nullable'],
            'file_type' => ['nullable'],
            'file' => ['nullable'],
        ];
    }
}
