<?php

namespace App\Http\ApiV1\Modules\Auth\Requests;

use App\Domain\Auth\Actions\Data\LoginRequestData;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class LoginByCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'phone' => ['required', 'string'],
            'code' => ['required', 'string'],
        ];
    }

    public function convertToObject(): LoginRequestData
    {
        return new LoginRequestData(
            null,
            null,
            $this->validated('phone'),
            $this->validated('code'),
        );
    }
}
