<?php

namespace App\Http\ApiV1\Modules\Auth\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;
use Ensi\CustomerAuthClient\Dto\RegisterValidationTypeEnum;

class RegisterRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['required', 'email'],
            'password' => ['nullable', 'required_if:validation_type,' . RegisterValidationTypeEnum::PASSWORD, 'string'],
            'code' => ['nullable', 'required_if:validation_type,' . RegisterValidationTypeEnum::CODE_BY_PHONE, 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['nullable', 'string'],
            'phone' => ['required', 'string', new PhoneRule()],
            'birthday' => ['nullable', 'date', 'date_format:Y-m-d', 'before:today'],
        ];
    }
}
