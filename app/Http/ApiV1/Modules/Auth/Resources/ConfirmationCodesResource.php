<?php

namespace App\Http\ApiV1\Modules\Auth\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponseData;

/** @mixin ConfirmationCodeResponseData */
class ConfirmationCodesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'seconds' => $this->getSeconds(),
            'is_exist' => $this->getIsExist(),
        ];
    }
}
