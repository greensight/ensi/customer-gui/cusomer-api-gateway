<?php

namespace App\Http\ApiV1\Modules\Auth\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class ConfirmationCodeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'phone' => $this->faker->numerify('+7##########'),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
