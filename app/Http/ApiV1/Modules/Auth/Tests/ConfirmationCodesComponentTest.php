<?php

use App\Domain\Auth\Models\Tests\Factories\ConfirmationCodeFactory;
use App\Http\ApiV1\Modules\Auth\Tests\Factories\ConfirmationCodeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/auth/confirmation-code 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomerAuthOauthApi()
        ->shouldReceive('createConfirmationCodeByPhone')
        ->andReturn(ConfirmationCodeFactory::new()->makeResponse());

    $request = ConfirmationCodeRequestFactory::new()->make();

    postJson('/api/v1/auth/confirmation-code', $request)
        ->assertOk();
});
