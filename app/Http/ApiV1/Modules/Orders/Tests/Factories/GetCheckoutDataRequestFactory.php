<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Domain\Orders\Tests\Oms\Factories\DeliveryAddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;

class GetCheckoutDataRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'delivery_method' => $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues()),
            'delivery_address' => DeliveryAddressFactory::new()->makeArray(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
