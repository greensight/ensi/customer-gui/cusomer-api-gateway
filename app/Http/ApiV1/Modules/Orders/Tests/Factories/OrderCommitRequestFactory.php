<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use App\Domain\Orders\Tests\Oms\Factories\DeliveryAddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\OmsClient\Dto\PaymentSystemEnum;

class OrderCommitRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            ...$this->generateDeliveryMethod(),
            "payment_system" => $this->faker->randomElement(PaymentSystemEnum::getAllowableEnumValues()),
            'client_comment' => $this->faker->nullable()->text(50),
        ];
    }

    public function make(array $extra = [])
    {
        return $this->makeArray($extra);
    }

    public function setDeliveryMethod(?int $deliveryMethod = null): self
    {
        return $this->state([
            ...$this->generateDeliveryMethod($deliveryMethod),
        ]);
    }

    protected function generateDeliveryMethod(?int $deliveryMethod = null): array
    {
        $deliveryMethod = $deliveryMethod ?? $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        return [
            "delivery_method" => $deliveryMethod,
            'delivery_comment' => $isDelivery ? $this->faker->nullable()->text(50) : null,
            'delivery_address' => $isDelivery ? DeliveryAddressFactory::new()->makeArray() : null,
        ];
    }
}
