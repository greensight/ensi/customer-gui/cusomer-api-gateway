<?php

namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use App\Domain\Crm\Actions\Favorites\EnrichFavoriteEntityAction;
use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\RequestBodyPagination;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrdersQuery extends QueryBuilder
{
    /** @var Collection<ElasticOffer> */
    protected Collection $offers;

    public function __construct(
        protected readonly OrdersApi $ordersApi,
        protected readonly ElasticOffersApi $offersApi,
        protected readonly EnrichFavoriteEntityAction $enrichFavoriteAction,
        Request $request
    ) {
        parent::__construct($request);
    }

    public function findOrFail($id): OrderData
    {
        $request = new SearchOrdersRequest();
        $request->setInclude(['items']);
        $request->setFilter((object)[
            'id' => $id,
            'customer_id' => user()->customerId,
        ]);
        $response = $this->ordersApi->searchOrders($request);

        if (empty($response->getData())) {
            throw new NotFoundHttpException();
        }

        return $this->convertFindToItem($response);
    }

    protected function convertFindToItem($response): OrderData
    {
        return current($this->convertArray($response->getData()));
    }

    protected function convertArray(array $orders): array
    {
        /** @var Collection|Order[] $orders */
        $orders = collect($orders);

        $offerIds = $orders->pluck('items')->collapse()->pluck('offer_id')->all();
        $this->loadOffers($offerIds)->wait();

        $offers = $this->offers->map(fn (ElasticOffer $offer) => new ElasticOfferData($offer));
        $offers = $this->enrichFavoriteAction->execute($offers);

        return OrderData::makeArray($orders, $offers);
    }

    protected function loadOffers(array $ids): PromiseInterface
    {
        $request = new SearchElasticOffersRequest();
        $request->setFilter((object)['offer_id' => $ids]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($ids)));
        $request->setInclude(['images']);

        return $this->offersApi->searchElasticOffersAsync($request)
            ->then(function (SearchElasticOffersResponse $response) {
                $this->offers = collect($response->getData())->keyBy(function (ElasticOffer $offer) {
                    return $offer->getId();
                });
            });
    }
}
