<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\Orders\OrderItemData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductImagesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin OrderItemData
 */
class CustomerOrderItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->orderItem->getId(),
            'offer_id' => $this->orderItem->getOfferId(),
            'name' => $this->orderItem->getName(),
            'code' => $this->offerData?->offer->getCode(),
            'barcode' => $this->offerData?->offer->getBarcode(),
            'vendor_code' => $this->offerData?->offer->getVendorCode(),
            'type' => $this->offerData?->offer->getType(),
            'qty' => $this->orderItem->getQty(),
            'price' => $this->orderItem->getPrice(),
            'is_adult' => $this->offerData?->offer->getIsAdult(),
            'is_favorite' => $this->offerData?->isFavorite(),
            $this->mergeWhen($this->offerData?->isWeight(), $this->getWeightOptions()),
            'images' => ProductImagesResource::collection($this->whenNotNull($this->offerData?->offer->getImages())),
        ];
    }

    public function getWeightOptions(): array
    {
        return ['weight_options' => [
            'uom' => $this->offerData?->offer->getUom(),
            'tariffing_volume' => $this->offerData?->offer->getTariffingVolume(),
            'order_step' => $this->offerData?->offer->getOrderStep(),
            'order_minvol' => $this->offerData?->offer->getOrderMinvol(),
        ]];
    }
}
