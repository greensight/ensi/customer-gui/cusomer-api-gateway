<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;

/**
 * @mixin OrderData
 */
class OrdersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $showPaymentLink = $this->order->getPaymentMethod() === PaymentMethodEnum::ONLINE
            && $this->order->getPaymentStatus() === PaymentStatusEnum::NOT_PAID;

        return [
            'id' => $this->order->getId(),
            'number' => $this->order->getNumber(),
            'client_comment' => $this->order->getClientComment(),
            'payment_method' => $this->order->getPaymentMethod(),
            'payment_system' => $this->order->getPaymentSystem(),
            'payment_status' => $this->order->getPaymentStatus(),
            'payment_url' => $this->when($showPaymentLink, $this->order->getPaymentLink()),
            'delivery_method' => $this->order->getDeliveryMethod(),
            'delivery_price' => $this->order->getDeliveryPrice(),
            'delivery_address' => DeliveryAddressResource::make($this->whenNotNull($this->order->getDeliveryAddress())),
            'promo_code' => $this->order->getPromoCode(),
            'cost' => $this->order->getCost(),
            'price' => $this->order->getPrice(),
            'qty' => count($this->order->getItems()),

            'items' => OrderItemsResource::collection($this->whenNotNull($this->getOrderItems())),
        ];
    }
}
