<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;

/**
 * @mixin Order
 */
class CustomerOrdersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $showPaymentLink = $this->getPaymentMethod() === PaymentMethodEnum::ONLINE && $this->getPaymentStatus() === PaymentStatusEnum::NOT_PAID;

        return [
            'id' => $this->getId(),
            'number' => $this->getNumber(),
            'price' => $this->getPrice(),
            'promo_code' => $this->getPromoCode(),

            'status' => $this->getStatus(),
            'status_at' => $this->dateTimeToIso($this->getStatusAt()),

            'client_comment' => $this->getClientComment(),
            'payment_method' => $this->getPaymentMethod(),
            'payment_system' => $this->getPaymentSystem(),
            'payment_status' => $this->getPaymentStatus(),
            'payment_link' => $this->when($showPaymentLink, $this->getPaymentLink()),

            'delivery_address' => DeliveryAddressResource::make($this->whenNotNull($this->getDeliveryAddress())),
            'delivery_method' => $this->getDeliveryMethod(),
            'delivery_price' => $this->getDeliveryPrice(),
            'delivery_comment' => $this->getDeliveryComment(),
        ];
    }
}
