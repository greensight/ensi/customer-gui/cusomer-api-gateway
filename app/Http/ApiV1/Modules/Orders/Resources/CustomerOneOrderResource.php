<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;

/**
 * @mixin OrderData
 */
class CustomerOneOrderResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $showPaymentLink = $this->order->getPaymentMethod() === PaymentMethodEnum::ONLINE && $this->order->getPaymentStatus() === PaymentStatusEnum::NOT_PAID;

        return [
            'id' => $this->order->getId(),
            'number' => $this->order->getNumber(),
            'client_comment' => $this->order->getClientComment(),
            'status' => $this->order->getStatus(),
            'status_at' => $this->dateTimeToIso($this->order->getStatusAt()),
            'payment_method' => $this->order->getPaymentMethod(),
            'payment_system' => $this->order->getPaymentSystem(),
            'payment_status' => $this->order->getPaymentStatus(),
            'payment_link' => $this->when($showPaymentLink, $this->order->getPaymentLink()),
            'delivery_method' => $this->order->getDeliveryMethod(),
            'delivery_price' => $this->order->getDeliveryPrice(),
            'delivery_comment' => $this->order->getDeliveryComment(),
            'delivery_address' => DeliveryAddressResource::make($this->whenNotNull($this->order->getDeliveryAddress())),
            'price' => $this->order->getPrice(),
            'promo_code' => $this->order->getPromoCode(),

            'order_items' => CustomerOrderItemsResource::collection($this->whenNotNull($this->getOrderItems())),
        ];
    }
}
