<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\CountryCodeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\OmsClient\Dto\PaymentSystemEnum;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CommitOrderRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_method' => ['required', 'integer', Rule::in(DeliveryMethodEnum::getAllowableEnumValues())],
            'payment_system' => ['required', 'integer', Rule::in(PaymentSystemEnum::getAllowableEnumValues())],

            'client_comment' => ['nullable', 'string'],
            'delivery_comment' => ['nullable', 'string'],

            'delivery_address' => [
                'nullable',
                'array',
                'required_if:delivery_method,' . DeliveryMethodEnum::DELIVERY,
                'prohibited_if:delivery_method,' . DeliveryMethodEnum::PICKUP,
            ],

            "delivery_address.address_string" => ['required_with:delivery_address', 'string'],
            "delivery_address.country_code" => ['required_with:delivery_address', 'string', new Enum(CountryCodeEnum::class)],
            "delivery_address.post_index" => ['required_with:delivery_address', 'string'],
            "delivery_address.region" => ['required_with:delivery_address', 'string'],
            "delivery_address.region_guid" => ['required_with:delivery_address', 'string'],
            "delivery_address.area" => ['nullable', 'string'],
            "delivery_address.area_guid" => ['nullable', 'string'],
            "delivery_address.city" => ['nullable', 'string'],
            "delivery_address.city_guid" => ['nullable', 'string'],
            "delivery_address.street" => ['nullable', 'string'],
            "delivery_address.house" => ['nullable', 'string'],
            "delivery_address.block" => ['nullable', 'string'],
            "delivery_address.flat" => ['nullable', 'string'],
            "delivery_address.floor" => ['nullable', 'string'],
            "delivery_address.porch" => ['nullable', 'string'],
            "delivery_address.intercom" => ['nullable', 'string'],
            "delivery_address.geo_lat" => ['required_with:delivery_address', 'string'],
            "delivery_address.geo_lon" => ['required_with:delivery_address', 'string'],
        ];
    }
}
