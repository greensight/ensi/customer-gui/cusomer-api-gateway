<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BasketsClient\Dto\Brand;

/**
 * @mixin Brand
 */
class ProductBrandsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
        ];
    }
}
