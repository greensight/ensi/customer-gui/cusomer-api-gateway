<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Modules\Catalog\Resources\Elastic\Data\AttributeValueData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\PropertyTypeEnum;

/**
 * @mixin AttributeValueData
 */
class ElasticAttributeValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'name' => $this->whenNotNull($this->value->getName()),
            'value' => $this->normalizedValue(),
        ];
    }

    public function normalizedValue(): float|bool|int|string
    {
        $value = $this->value->getValue();

        return match ($this->attribute->getType()) {
            PropertyTypeEnum::DOUBLE => (float)$value,
            PropertyTypeEnum::BOOLEAN => (bool)$value,
            PropertyTypeEnum::INTEGER => (int)$value,
            default => $value
        };
    }
}
