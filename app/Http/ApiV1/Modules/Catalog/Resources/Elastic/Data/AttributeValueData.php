<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic\Data;

use Ensi\CatalogCacheClient\Dto\AttributeValue;
use Ensi\CatalogCacheClient\Dto\ElasticAttribute;

class AttributeValueData
{
    public function __construct(
        public readonly ElasticAttribute $attribute,
        public readonly AttributeValue $value,
    ) {
    }
}
