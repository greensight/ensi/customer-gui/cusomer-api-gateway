<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\Gluing;

/**
 * @mixin Gluing
 */
class ElasticGluingResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'props' => ElasticGluingPropertiesResource::collection($this->whenNotNull($this->getProps())),
        ];
    }
}
