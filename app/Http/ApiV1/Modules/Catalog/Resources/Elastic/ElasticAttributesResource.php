<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Modules\Catalog\Resources\Elastic\Data\AttributeValueData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\AttributeValue;
use Ensi\CatalogCacheClient\Dto\ElasticAttribute;

/**
 * @mixin ElasticAttribute
 */
class ElasticAttributesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'values' => ElasticAttributeValuesResource::collection(
                array_map(
                    fn (AttributeValue $attributeValue) => new AttributeValueData($this->resource, $attributeValue),
                    $this->getValues()
                )
            ),
        ];
    }
}
