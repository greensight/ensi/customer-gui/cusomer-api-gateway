<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ElasticBrand;

/**
 * @mixin ElasticBrand
 */
class ElasticBrandsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'description' => $this->getDescription(),
            'logo_file' => $this->fileUrl($this->getLogoFile()) ?? $this->getLogoUrl(),
        ];
    }
}
