<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ElasticOfferData
 */
class ElasticOffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->offer->getId(),
            'product_id' => $this->offer->getProductId(),

            'main_image' => $this->offer->getMainImageFile()?->getUrl(),

            'category_ids' => $this->offer->getCategoryIds(),
            'brand_id' => $this->offer->getBrandId(),

            'code' => $this->offer->getCode(),
            'name' => $this->offer->getName(),
            'description' => $this->offer->getDescription(),
            'type' => $this->offer->getType(),
            'vendor_code' => $this->offer->getVendorCode(),
            'barcode' => $this->offer->getBarcode(),

            'weight' => $this->offer->getWeight(),
            'weight_gross' => $this->offer->getWeightGross(),
            'length' => $this->offer->getLength(),
            'height' => $this->offer->getHeight(),
            'width' => $this->offer->getWidth(),
            'is_adult' => $this->offer->getIsAdult(),
            'is_favorite' => $this->isFavorite(),

            'price' => $this->offer->getPrice(),
            'cost' => $this->offer->getCost(),

            'gluing_name' => $this->offer->getGluingName(),
            'gluing_is_main' => $this->offer->getGluingIsMain(),
            'gluing_is_active' => $this->offer->getGluingIsActive(),

            $this->mergeWhen($this->isWeight(), $this->getWeightOptions()),

            'discount' => ElasticDiscountsResource::make($this->whenNotNull($this->offer->getDiscount())),
            'categories' => ElasticCategoriesResource::collection($this->whenNotNull($this->offer->getCategories())),
            'brand' => ElasticBrandsResource::make($this->whenNotNull($this->offer->getBrand())),
            'images' => ElasticImagesResource::collection($this->whenNotNull($this->offer->getImages())),
            'attributes' => ElasticAttributesResource::collection($this->whenNotNull($this->offer->getAttributes())),
            'gluing' => ElasticGluingResource::collection($this->whenNotNull($this->offer->getGluing())),
            'nameplates' => ElasticNameplatesResource::collection($this->whenNotNull($this->offer->getNameplates())),
        ];
    }

    public function getWeightOptions(): array
    {
        return ['weight_options' => [
            'uom' => $this->offer->getUom(),
            'tariffing_volume' => $this->offer->getTariffingVolume(),
            'order_step' => $this->offer->getOrderStep(),
            'order_minvol' => $this->offer->getOrderMinvol(),
        ]];
    }
}
