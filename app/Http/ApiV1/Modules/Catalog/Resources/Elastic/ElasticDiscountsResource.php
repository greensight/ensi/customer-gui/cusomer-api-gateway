<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ElasticDiscount;

/** @mixin ElasticDiscount */
class ElasticDiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'value' => $this->getValue(),
            'value_type' => $this->getValueType(),
        ];
    }
}
