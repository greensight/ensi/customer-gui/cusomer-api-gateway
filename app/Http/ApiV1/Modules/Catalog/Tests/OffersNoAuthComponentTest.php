<?php

use App\Domain\Auth\Models\User;
use App\Domain\Catalog\Tests\Factories\Cloud\CatalogSearchFactory;
use App\Domain\Catalog\Tests\Factories\Feed\CloudIntegrationFactory;
use App\Http\ApiV1\Support\Tests\ApiV1NoAuthComponentTestCase;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\CatalogSearchRequest;

use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1NoAuthComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/offers:search X-Customer-Id header', function (bool $auth) {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $userId = null;
    if ($auth) {
        $user = User::factory()->make();
        $this->actingAs($user);
        $userId = $user->customerId;
    }

    $this->mockFeedCloudIntegrationsApi()
        ->shouldReceive('getCloudIntegration')
        ->once()
        ->andReturn(CloudIntegrationFactory::new()->active()->makeResponse());

    $searchRequest = null;
    $this->mockCloudSdkApi()
        ->shouldReceive('search')
        ->withArgs(function (CatalogSearchRequest $request) use (&$searchRequest) {
            $searchRequest = $request;

            return true;
        })
        ->andReturn(CatalogSearchFactory::new()->withProducts([])->makeResponse());

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->never();

    postJson('/api/v1/catalog/offers:search', ['filter' => ['name' => 'foo']])
        ->assertOk()
        ->assertJsonCount(0, 'data');

    assertEquals($userId, $searchRequest->customerId);
})->with([true, false]);
