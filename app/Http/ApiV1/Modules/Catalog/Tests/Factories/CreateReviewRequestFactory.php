<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateReviewRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'product_id' => $this->faker->modelId(),
            'comment' => $this->faker->optional()->text,
            'grade' => $this->faker->numberBetween(1, 5),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
