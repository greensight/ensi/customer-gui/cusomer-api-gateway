<?php

use App\Domain\Catalog\Tests\Factories\Categories\CategoriesTreeItemFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/catalog/categories success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimCategoriesApi()->allows([
        'getCategoriesTree' => CategoriesTreeItemFactory::new()
            ->withChildren(2)
            ->depth(2)
            ->makeResponse(2),
    ]);

    getJson('/api/v1/catalog/categories?parent_id=1')
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonCount(2, 'data.0.children');
});
