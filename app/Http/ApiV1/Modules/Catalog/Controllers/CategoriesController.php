<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Http\ApiV1\Modules\Catalog\Queries\Categories\CategoriesTreeQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Categories\CategoriesTreeRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesTreeResource;

class CategoriesController
{
    public function tree(CategoriesTreeRequest $request, CategoriesTreeQuery $query)
    {
        return CategoriesTreeResource::collection($query->get());
    }
}
