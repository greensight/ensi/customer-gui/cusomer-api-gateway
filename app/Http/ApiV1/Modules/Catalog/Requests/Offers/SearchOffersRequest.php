<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Offers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SearchOffersRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'filter.name' => ['required', 'string'],
            'filter.gluing_is_main' => ['boolean'],
            'filter.category_id' => ['integer'],
            'filter.brand_id' => ['integer'],

            'pagination.limit' => ['integer', 'min:1', 'max:1000'],
            'pagination.offset' => ['integer', 'min:0'],

            'include' => ['array'],
            'include.*' => ['string'],
        ];
    }
}
