<?php

namespace App\Http\ApiV1\Support\Requests;

class CommonFilterEnumValuesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'filter' => ['array'],
            'filter.query' => ['required_without:filter.id', 'string'],
            'filter.id' => ['required_without:filter.query', 'array'],
        ];
    }
}
