<?php

namespace App\Http\ApiV1\Support\Tests;

use App\Domain\Auth\Models\User;

abstract class ApiV1NoAuthComponentTestCase extends ApiV1ComponentTestCase
{
    protected function authorize(User $user = null)
    {
        //
    }
}
