<?php

namespace App\Http\ApiV1\Support\Tests;

use App\Domain\Auth\Models\User;
use App\Domain\Common\Tests\Factories\JWTFactory;
use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Tests\ComponentTestCase;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->authorize();
    }

    protected function authorize(User $user = null)
    {
        $this->actingAs($user ?? User::factory()->make());
        $this->withHeaders([config('auth.header_for_token') => JWTFactory::new()->make()]);
    }
}
