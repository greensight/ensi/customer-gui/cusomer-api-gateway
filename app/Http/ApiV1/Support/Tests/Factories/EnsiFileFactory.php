<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use Ensi\LaravelEnsiFilesystem\EnsiStorageConfig;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Support\Facades\Storage;

class EnsiFileFactory extends BaseApiFactory
{
    protected string $path = 'model_dir/subdir/file.ext';
    protected string $visible = 'public';

    protected function definition(): array
    {
        return [
            'path' => $this->path,
            'root_path' => "{$this->visible}/domain/{$this->path}",
            'rootPath' => "{$this->visible}/domain/{$this->path}", // fixme #80451
            'url' => "https://storage.ru/domain/{$this->path}",
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeReal(?string $disk = null, array $extra = []): array
    {
        $disk = $disk ?: EnsiStorageConfig::rootDiskName();
        $arr = $this->make($extra);
        Storage::fake($disk);
        Storage::disk($disk)->put($arr['root_path'], 'file');

        return $arr;
    }

    public function protected(): self
    {
        $this->visible = 'protected';

        return $this;
    }

    public function fileName(string $fileName): self
    {
        $this->path = pathinfo($this->path, PATHINFO_DIRNAME) . '/' . $fileName;

        return $this;
    }

    public function fileExt(string $ext): self
    {
        $this->path = pathinfo($this->path, PATHINFO_DIRNAME) . '/' . pathinfo($this->path, PATHINFO_BASENAME) . $ext;

        return $this;
    }
}
