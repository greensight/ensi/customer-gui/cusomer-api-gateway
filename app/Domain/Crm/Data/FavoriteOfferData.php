<?php

namespace App\Domain\Crm\Data;

use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\PimClient\Dto\ProductTypeEnum;

class FavoriteOfferData
{
    public function __construct(public readonly ElasticOffer $offer)
    {
    }

    public function isFavorite(): bool
    {
        return true;
    }

    public function isWeight(): bool
    {
        return $this->offer->getType() === ProductTypeEnum::WEIGHT;
    }
}
