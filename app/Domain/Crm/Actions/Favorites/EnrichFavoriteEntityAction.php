<?php

namespace App\Domain\Crm\Actions\Favorites;

use App\Domain\Auth\Models\User;
use App\Domain\Crm\Contracts\FavoriteEntity;
use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\PaginationTypeEnum;
use Ensi\CrmClient\Dto\RequestBodyPagination;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest;
use Illuminate\Support\Collection;

class EnrichFavoriteEntityAction
{
    protected Collection $customerFavorites;
    protected ?User $user;

    public function __construct(protected readonly CustomerFavoritesApi $favoritesApi)
    {
        $this->customerFavorites = collect();
    }

    public function execute(Collection $entities): Collection
    {
        $this->user = user();
        if ($this->user === null) {
            return $entities;
        }

        $productIds = $entities->map(fn (FavoriteEntity $entity) => $entity->getLinkFavoriteId())->unique()->all();

        if (filled($productIds)) {
            $this->loadOffers($productIds);
        }

        return $entities->each(function (FavoriteEntity $entity) {
            if ($this->customerFavorites->has($entity->getLinkFavoriteId())) {
                $entity->markAsFavorite();
            }
        });
    }

    protected function loadOffers(array $productIds): void
    {
        $request = new SearchCustomerFavoritesRequest();
        $request->setFilter((object)['customer_id' => $this->user->customerId, 'product_id' => $productIds]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($productIds))->setType(PaginationTypeEnum::CURSOR));

        $this->customerFavorites = collect($this->favoritesApi->searchCustomerFavorite($request)->getData())->keyBy('product_id');
    }
}
