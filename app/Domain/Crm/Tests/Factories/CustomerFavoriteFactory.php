<?php

namespace App\Domain\Crm\Tests\Factories;

use Ensi\CrmClient\Dto\CustomerFavorite;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerFavoriteFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): CustomerFavorite
    {
        return new CustomerFavorite($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCustomerFavoritesResponse
    {
        return $this->generateResponseSearch(SearchCustomerFavoritesResponse::class, $extras, $count, $pagination);
    }
}
