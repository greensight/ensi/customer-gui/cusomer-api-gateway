<?php

namespace App\Domain\Crm\Traits;

trait Favorite
{
    protected bool $isFavorite = false;

    public function markAsFavorite(): void
    {
        $this->isFavorite = true;
    }

    public function isFavorite(): bool
    {
        return $this->isFavorite;
    }
}
