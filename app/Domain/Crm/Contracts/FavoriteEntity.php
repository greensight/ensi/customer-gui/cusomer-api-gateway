<?php

namespace App\Domain\Crm\Contracts;

interface FavoriteEntity
{
    public function getLinkFavoriteId(): int;

    public function markAsFavorite(): void;

    public function isFavorite(): bool;
}
