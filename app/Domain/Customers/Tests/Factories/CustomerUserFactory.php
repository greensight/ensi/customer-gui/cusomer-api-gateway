<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CustomerAuthClient\Dto\SearchUsersResponse;
use Ensi\CustomerAuthClient\Dto\User;
use Ensi\CustomerAuthClient\Dto\UserResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerUserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'login' => $this->faker->email(),
            'active' => $this->faker->boolean(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->lastName(),
            'full_name' => $this->faker->name(),
            'short_name' => $this->faker->name(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): User
    {
        return new User($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): UserResponse
    {
        return new UserResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchUsersResponse
    {
        return $this->generateResponseSearch(SearchUsersResponse::class, $extras, $count, $pagination);
    }
}
