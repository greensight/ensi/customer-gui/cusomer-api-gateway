<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\CustomerGenderEnum;
use Ensi\CustomersClient\Dto\CustomerResponse;
use Ensi\CustomersClient\Dto\SearchCustomersResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'user_id' => $this->faker->modelId(),
            'status_id' => $this->faker->word,
            'manager_id' => $this->faker->modelId(),
            'yandex_metric_id' => $this->faker->modelId(),
            'google_analytics_id' => $this->faker->modelId(),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->unique()->phoneNumber(),
            'first_name' => $this->faker->text(10),
            'last_name' => $this->faker->text(10),
            'middle_name' => $this->faker->text(10),
            'gender' => $this->faker->randomElement(CustomerGenderEnum::getAllowableEnumValues()),
            'create_by_admin' => $this->faker->boolean(),
            'city' => $this->faker->text(),
            'birthday' => $this->faker->date(),
            'comment_status' => $this->faker->text(),
            'timezone' => $this->faker->timezone(),
            'avatar' => $this->faker->url(),
            'new_email' => $this->faker->unique()->email(),
        ];
    }

    public function make(array $extra = []): Customer
    {
        return new Customer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CustomerResponse
    {
        return new CustomerResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCustomersResponse
    {
        return $this->generateResponseSearch(SearchCustomersResponse::class, $extras, $count, $pagination);
    }
}
