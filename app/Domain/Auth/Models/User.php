<?php

namespace App\Domain\Auth\Models;

use App\Domain\Auth\Models\Tests\Factories\UserFactory;
use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public function __construct(
        public readonly int $id,
        public readonly int $customerId,
        public readonly ?string $login,
        public readonly ?string $fullName,
        public readonly ?string $lastName,
        public readonly ?string $firstName,
        public readonly ?string $middleName,
        public readonly ?string $email,
        public readonly ?string $phone,
        public readonly ?bool $active,
        public ?string $rememberToken,
    ) {
    }

    /**
     * Возвращает логин пользователя.
     */
    public function getAuthIdentifierName(): string
    {
        return $this->login;
    }

    /**
     * Возвращает идентификатор пользователя.
     */
    public function getAuthIdentifier(): int
    {
        return $this->id;
    }

    /**
     * Заглушка для пароля.
     */
    public function getAuthPassword(): string
    {
        return '';
    }

    /**
     * Возвращает токен авторизации.
     */
    public function getRememberToken(): string
    {
        return $this->rememberToken ?? '';
    }

    /**
     * Устанавливает токен авторизации.
     *
     * @param string $value
     */
    public function setRememberToken($value): void
    {
        $this->rememberToken = $value;
    }

    /**
     * Возвращает имя токена авторизации.
     */
    public function getRememberTokenName(): string
    {
        return 'remember_token';
    }

    public function getAuthPasswordName(): string
    {
        return '';
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }
}
