<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponse;
use Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponseData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ConfirmationCodeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'seconds' => $this->faker->numberBetween(60, 90),
            'is_exist' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): ConfirmationCodeResponseData
    {
        return new ConfirmationCodeResponseData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ConfirmationCodeResponse
    {
        return new ConfirmationCodeResponse(['data' => $this->make($extra)]);
    }
}
