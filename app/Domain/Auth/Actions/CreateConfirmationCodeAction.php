<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Auth\Exceptions\AuthUserException;
use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponseData;
use Ensi\CustomerAuthClient\Dto\CreateConfirmationCodeByPhoneRequest;

class CreateConfirmationCodeAction
{
    public function __construct(
        protected OauthApi $oauthApi,
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): ConfirmationCodeResponseData
    {
        $request = new CreateConfirmationCodeByPhoneRequest($fields);

        $responseData = $this->oauthApi->createConfirmationCodeByPhone($request)->getData();

        if (!empty($responseData->getMessage())) {
            throw new AuthUserException($responseData->getMessage(), $responseData->getSeconds(), $responseData->getCode());
        }

        return $responseData;
    }
}
