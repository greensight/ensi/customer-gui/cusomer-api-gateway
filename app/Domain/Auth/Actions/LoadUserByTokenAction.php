<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Auth\Models\User;
use App\Domain\Customers\Actions\LoadCustomerByUserIdAction;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;

class LoadUserByTokenAction
{
    public function __construct(
        private readonly UsersApi $usersApi,
        private readonly LoadCustomerByUserIdAction $loadCustomerByUserIdAction,
    ) {
    }

    public function execute(string $token): ?User
    {
        try {
            $this->usersApi->getConfig()->setAccessToken($token);
            $response = $this->usersApi->getCurrentUser();

            $user = $response->getData();
            $customer = $this->loadCustomerByUserIdAction->execute($user->getId());

            return new User(
                id: $user->getId(),
                customerId: $customer->getId(),
                login: $user->getLogin(),
                fullName: $customer->getFullName(),
                lastName: $customer->getLastName(),
                firstName: $customer->getFirstName(),
                middleName: $customer->getMiddleName(),
                email: $customer->getEmail(),
                phone: $customer->getPhone(),
                active: $user->getActive(),
                rememberToken: $token,
            );
        } catch (ApiException $e) {
            return null;
        }
    }
}
