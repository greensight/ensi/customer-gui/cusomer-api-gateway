<?php

namespace App\Domain\Communication\Tests\Factories;

use Ensi\CommunicationManagerClient\Dto\Notification;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NotificationFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'event' => $this->faker->randomElement(NotificationEventEnum::getAllowableEnumValues()),
            'channels' => $this->faker->randomList(fn () => $this->faker->randomElement(NotificationChannelEnum::getAllowableEnumValues()), min: 1),
            'theme' => $this->faker->nullable()->sentence(3),
            'text' => $this->faker->text(),
            'is_viewed' => $this->faker->boolean(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Notification
    {
        return new Notification($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchNotificationsResponse
    {
        return $this->generateResponseSearch(SearchNotificationsResponse::class, $extra, $count);
    }
}
