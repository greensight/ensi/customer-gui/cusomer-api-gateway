<?php

namespace App\Domain\Common\ProtectedFiles;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProtectedFileLoaderFactory
{
    public function build(ProtectedFile $file): ProtectedFileLoader
    {
        /** @var ProtectedFileLoader $loader */
        $loader = match ($file::entity()) {
            default => throw new ModelNotFoundException("Не удалось определить сущность"),
        };

        /**
         * @todo delete phpstan-ignore-next-line after you add implementation ProtectedFile
         * @phpstan-ignore-next-line
         */
        $loader->setFile($file);

        return $loader;
    }
}
