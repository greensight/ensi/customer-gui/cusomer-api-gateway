<?php

namespace App\Domain\Common\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class BaseAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'address_string' => $this->faker->address(),
            'post_index' => $this->faker->postcode(),
            'country_code' => 'RU',
            'region' => $this->faker->city(),
            'region_guid' => $this->faker->uuid(),
            'area' => $this->faker->city(),
            'area_guid' => $this->faker->uuid(),
            'city' => $this->faker->city(),
            'city_guid' => $this->faker->uuid(),
            'street' => $this->faker->streetAddress(),
            'house' => $this->faker->buildingNumber(),
            'block' => $this->faker->numerify('##'),
            'flat' => $this->faker->numerify('##'),
            'floor' => $this->faker->numerify('##'),
            'porch' => $this->faker->numerify('##'),
            'intercom' => $this->faker->numerify('##-##'),
            'geo_lat' => (string)$this->faker->latitude(),
            'geo_lon' => (string)$this->faker->longitude(),
        ];
    }

    public function make(array $extra = [])
    {
        return $this->makeArray($extra);
    }
}
