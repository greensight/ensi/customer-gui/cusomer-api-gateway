<?php

namespace App\Domain\Cms\Tests\Factories\Pages;

use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\CmsClient\Dto\Banner;
use Ensi\CmsClient\Dto\BannerButton;
use Ensi\CmsClient\Dto\File;
use Ensi\CmsClient\Dto\SearchBannersResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Support\Str;

class BannerFactory extends BaseApiFactory
{
    protected ?BannerButton $button = null;

    protected function definition(): array
    {
        $name = $this->faker->words(3, true);

        $data = [
            'id' => $this->faker->modelId(),
            'name' => $name,
            'code' => Str::slug($name),
            'is_active' => $this->faker->boolean,
            'desktop_image' => $this->faker->boolean ? $this->generateFile() : null,
            'mobile_image' => $this->faker->boolean ? $this->generateFile() : null,
            'url' => $this->faker->optional()->url,
        ];

        if ($this->button) {
            $data['button'] = $this->button;
        }

        return $data;
    }

    public function withButton(BannerButton $button): static
    {
        $this->button = $button;

        return $this;
    }

    public function make(array $extra = []): Banner
    {
        return new Banner($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchBannersResponse
    {
        return $this->generateResponseSearch(SearchBannersResponse::class, $extra, $count);
    }

    protected function generateFile(): File
    {
        return new File(EnsiFileFactory::new()->make());
    }
}
