<?php

namespace App\Domain\Cms\Tests\Factories\Pages;

use App\Http\ApiV1\OpenApiGenerated\Enums\CmsBannerButtonLocationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\CmsBannerButtonTypeEnum;
use Ensi\CmsClient\Dto\BannerButton;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerButtonFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'url' => $this->faker->url,
            'text' => $this->faker->sentence,
            'location' => $this->faker->randomEnum(CmsBannerButtonLocationEnum::cases()),
            'type' => $this->faker->randomEnum(CmsBannerButtonTypeEnum::cases()),
        ];
    }

    public function make(array $extra = []): BannerButton
    {
        return new BannerButton($this->makeArray($extra));
    }
}
