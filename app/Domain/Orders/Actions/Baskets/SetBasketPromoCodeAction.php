<?php

namespace App\Domain\Orders\Actions\Baskets;

use App\Domain\Crm\Actions\Favorites\EnrichFavoriteEntityAction;
use App\Domain\Orders\Data\Baskets\CalculateBasketData;
use Ensi\BasketsClient\Api\CalculatorsBasketsApi;
use Ensi\BasketsClient\Dto\SetBasketPromoCodeRequest;

class SetBasketPromoCodeAction
{
    public function __construct(
        protected readonly CalculatorsBasketsApi $calculatorsBasketsApi,
        protected readonly EnrichFavoriteEntityAction $enrichFavoriteEntityAction,
    ) {
    }

    public function execute(?string $promoCode): CalculateBasketData
    {
        $request = new SetBasketPromoCodeRequest();
        $request->setCustomerId(user()->customerId);
        $request->setPromoCode($promoCode);

        $basket = $this->calculatorsBasketsApi->setBasketCalculatePromoCode($request)->getData();

        $basketData = CalculateBasketData::make($basket);
        $this->enrichFavoriteEntityAction->execute($basketData->getItems());

        return $basketData;
    }
}
