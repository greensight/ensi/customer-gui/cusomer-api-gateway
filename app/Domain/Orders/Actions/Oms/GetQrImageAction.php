<?php

namespace App\Domain\Orders\Actions\Oms;

use App\Domain\Common\Exceptions\AccessException;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\Order;
use Storage;

class GetQrImageAction
{
    public function __construct(
        protected readonly OrdersApi $ordersApi,
        protected readonly EnsiFilesystemManager $filesystemManager
    ) {
    }

    public function execute(int $orderId): string
    {
        $order = $this->ordersApi->getOrder($orderId)->getData();

        if (!$this->isAccessToOrderByUser($order)) {
            throw new AccessException('Forbidden');
        }

        $qrCode = $order->getPaymentData()->getQrCode();

        if (!$qrCode) {
            throw new ApiException('QR-code not found', 404);
        }

        return Storage::disk($this->filesystemManager->rootDiskName())->path($qrCode->getRootPath());
    }

    private function isAccessToOrderByUser(Order $order): bool
    {
        return user()->customerId === $order->getCustomerId();
    }
}
