<?php

namespace App\Domain\Orders\Actions\Oms;

use App\Exceptions\ValidateException;
use Ensi\BasketsClient\Api\CalculatorsBasketsApi;
use Ensi\BasketsClient\Dto\CalculateBasket;
use Ensi\BasketsClient\Dto\SearchBasketCalculateRequest;
use Ensi\BasketsClient\Dto\SearchBasketCalculateResponse;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\RequestBodyPagination;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryPriceResponse;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Address;
use Ensi\OmsClient\Dto\OrderCommitRequest;
use Ensi\OmsClient\Dto\OrderCommitRequestDeliveries;
use Ensi\OmsClient\Dto\OrderCommitRequestItems;
use Ensi\OmsClient\Dto\OrderCommitRequestShipments;
use Ensi\OmsClient\Dto\OrderCommitResponseData;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;

class CommitOrderAction
{
    protected ?Store $store = null;
    protected ?CalculateBasket $basket = null;
    protected ?int $deliveryPrice = 0;

    public function __construct(
        protected readonly OrdersApi $ordersApi,
        protected readonly CalculatorsBasketsApi $basketsApi,
        protected readonly StoresApi $storesApi,
        protected readonly DeliveryPricesApi $deliveryPricesApi,
    ) {
    }

    public function execute(array $data): OrderCommitResponseData
    {
        Utils::unwrap(array_filter([
            $this->loadStore(),
            $this->loadBasket(),
            $this->loadDeliveryPrice($data),
        ]));
        if (!$this->store) {
            throw new ValidateException("В системе нет ни одного склада");
        }
        if (!$this->basket) {
            throw new ValidateException("Не удалось получить корзину");
        }
        if (is_null($this->deliveryPrice)) {
            throw new ValidateException("Не удалось найти стоимость доставки");
        }

        $request = new OrderCommitRequest();
        $request->setPaymentSystemId($data['payment_system']);
        $request->setSource(OrderSourceEnum::SITE);
        $request->setDeliveryMethod($data['delivery_method']);
        $request->setDeliveryPrice($this->deliveryPrice);
        $request->setClientComment($data['client_comment'] ?? null);
        $request->setDeliveryComment($data['delivery_comment'] ?? null);
        $deliveryAddress = $data['delivery_address'] ?? null;
        $request->setDeliveryAddress($deliveryAddress ? new Address($deliveryAddress) : null);
        $request->setCustomerId(user()->customerId);
        $request->setPromoCode($this->basket->getPromoCode());
        $request->setCustomerEmail(user()->email);
        $request->setReceiverName(user()->fullName);
        $request->setReceiverEmail(user()->email);
        $request->setReceiverPhone(user()->phone);

        $shipment = new OrderCommitRequestShipments();
        $shipment->setItems($this->getOrderItems());
        $shipment->setStoreId($this->store->getId());

        $delivery = new OrderCommitRequestDeliveries();
        $delivery->setShipments([$shipment]);

        $request->setDeliveries([$delivery]);

        return $this->ordersApi->commitOrder($request)->getData();
    }

    private function getOrderItems(): array
    {
        $orderItems = [];
        foreach ($this->basket->getItems() ?? [] as $item) {
            $orderItem = new OrderCommitRequestItems();
            $orderItem->setOfferId($item->getOfferId());
            $orderItem->setQty($item->getQty());
            $orderItem->setCostPerOne($item->getCostPerOne());
            $orderItem->setPricePerOne($item->getPricePerOne());

            $orderItems[] = $orderItem;
        }

        return $orderItems;
    }

    protected function loadStore(): PromiseInterface
    {
        $request = new SearchStoresRequest();
        $request->setPagination((new RequestBodyPagination())->setLimit(1));

        return $this->storesApi->searchStoresAsync($request)->then(function (SearchStoresResponse $response) {
            $this->store = $response->getData()[0] ?? null;
        });
    }

    protected function loadBasket(): PromiseInterface
    {
        $request = new SearchBasketCalculateRequest();
        $request->setCustomerId(user()->customerId);
        $request->setInclude(['items']);

        return $this->basketsApi->searchBasketCalculateAsync($request)->then(function (SearchBasketCalculateResponse $response) {
            $this->basket = $response->getData();
        });
    }

    private function loadDeliveryPrice(array $data): ?PromiseInterface
    {
        if (empty($data['delivery_address']['region_guid'])) {
            return null;
        }

        $request = new SearchDeliveryPricesRequest();
        $request->setFilter((object)[
            'region_guid' => $data['delivery_address']['region_guid'],
            'delivery_method' => $data['delivery_method'],
        ]);

        return $this->deliveryPricesApi->searchDeliveryPriceAsync($request)->then(function (DeliveryPriceResponse $response) {
            $this->deliveryPrice = $response->getData()->getPrice();
        })->otherwise(function (ApiException $e) {
            if ($e->getCode() == 404) {
                $this->deliveryPrice = null;

                return null;
            }

            throw $e;
        });
    }
}
