<?php

namespace App\Domain\Orders\Data\Orders;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use Ensi\OmsClient\Dto\OrderItem;

class OrderItemData
{
    public function __construct(public OrderItem $orderItem, public ?ElasticOfferData $offerData)
    {
    }
}
