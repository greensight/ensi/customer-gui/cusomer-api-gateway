<?php

namespace App\Domain\Orders\Data\Orders;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use Ensi\OmsClient\Dto\Order;
use Illuminate\Support\Collection;

class OrderData
{
    /** @var OrderItemData[] */
    protected array $items = [];

    public function __construct(public readonly Order $order)
    {
    }

    public function addOrderItem(OrderItemData $item): void
    {
        $this->items[] = $item;
    }

    public function getOrderItems(): array
    {
        return $this->items;
    }

    /**
     * @param Collection<Order> $orders
     * @param Collection<ElasticOfferData> $offers
     */
    public static function makeArray(Collection $orders, Collection $offers): array
    {
        $ordersData = [];
        foreach ($orders as $order) {
            $orderData = new OrderData($order);
            $orderItems = $order->getItems();

            foreach ($orderItems as $orderItem) {
                $offer = $offers->get($orderItem->getOfferId());
                $orderItemData = new OrderItemData($orderItem, $offer);

                $orderData->addOrderItem($orderItemData);
            }

            $ordersData[] = $orderData;
        }

        return $ordersData;
    }
}
