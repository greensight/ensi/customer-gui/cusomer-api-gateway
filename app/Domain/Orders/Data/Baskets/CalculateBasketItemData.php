<?php

namespace App\Domain\Orders\Data\Baskets;

use App\Domain\Crm\Contracts\FavoriteEntity;
use App\Domain\Crm\Traits\Favorite;
use Ensi\BasketsClient\Dto\CalculateBasketItem;
use Ensi\PimClient\Dto\ProductTypeEnum;

class CalculateBasketItemData implements FavoriteEntity
{
    use Favorite;

    public function __construct(public CalculateBasketItem $item)
    {
    }

    public function getLinkFavoriteId(): int
    {
        return $this->item->getProductId();
    }

    public function isWeight(): bool
    {
        return $this->item->getType() === ProductTypeEnum::WEIGHT;
    }
}
