<?php

namespace App\Domain\Orders\Tests\Oms\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\PaymentSystemEnum;
use Ensi\OmsClient\Dto\SearchOrdersResponse;

class OrderFactory extends BaseApiFactory
{
    protected array $items = [];

    protected function definition(): array
    {
        $price = $this->faker->numberBetween(1000, 10000);
        $paymentStatus = $this->faker->randomElement(PaymentStatusEnum::getAllowableEnumValues());
        $paymentMethod = $this->faker->randomElement(PaymentMethodEnum::getAllowableEnumValues());
        $showPaymentLink = $paymentMethod === PaymentMethodEnum::ONLINE && $paymentStatus === PaymentStatusEnum::NOT_PAID;

        $definition = [
            'id' => $this->faker->modelId(),
            'number' => $this->faker->unique()->numerify('######'),
            'client_comment' => $this->faker->nullable()->text(50),
            'payment_method' => $paymentMethod,
            'payment_system' => $this->faker->randomElement(PaymentSystemEnum::getAllowableEnumValues()),
            'payment_status' => $paymentStatus,
            'delivery_method' => $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues()),
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'price' => $price,
            'promo_code' => $this->faker->nullable()->word(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'status' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'delivery_price' => $this->faker->numberBetween($price, $price + 10000),
            'payment_link' => $showPaymentLink ? $this->faker->url() : null,
            'delivery_comment' => $this->faker->text(50),
            'delivery_address' => DeliveryAddressFactory::new()->make(),
        ];

        if ($this->items) {
            $definition['items'] = $this->items;
        }

        return $definition;
    }

    public function make(array $extra = []): Order
    {
        return new Order($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOrdersResponse
    {
        return $this->generateResponseSearch(SearchOrdersResponse::class, $extras, $count, $pagination);
    }

    public function withItems(?OrderItem $item = null): self
    {
        $this->items[] = $item ?: OrderItemFactory::new()->make();

        return $this;
    }
}
