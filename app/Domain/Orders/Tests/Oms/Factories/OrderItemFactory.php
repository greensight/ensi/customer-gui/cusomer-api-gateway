<?php

namespace App\Domain\Orders\Tests\Oms\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderItem;

class OrderItemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $price = $this->faker->numberBetween(1, 10000);
        $pricePerOne = $this->faker->numberBetween(1, $price);

        return [
            'id' => $this->faker->modelId(),
            'order_id' => $this->faker->modelId(),
            'shipment_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'name' => $this->faker->word,
            'qty' => $this->faker->randomFloat(2, 1, 100),
            'price' => $price,
            'price_per_one' => $pricePerOne,
            'cost' => $this->faker->numberBetween(1, $price + 10000),
            'cost_per_one' => $this->faker->numberBetween(1, $pricePerOne + 10000),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): OrderItem
    {
        return new OrderItem($this->makeArray($extra));
    }
}
