<?php

namespace App\Domain\Orders\Tests\Oms\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\CountryCodeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Address;

class DeliveryAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'address_string' => $this->faker->address(),
            'country_code' => $this->faker->randomEnum(CountryCodeEnum::cases()),
            'post_index' => $this->faker->postcode(),
            'region' => $this->faker->city(),
            'region_guid' => $this->faker->uuid(),
            'area' => $this->faker->nullable()->city(),
            'area_guid' => $this->faker->nullable()->uuid(),
            'city' => $this->faker->nullable()->city(),
            'city_guid' => $this->faker->nullable()->uuid(),
            'street' => $this->faker->nullable()->streetAddress(),
            'house' => $this->faker->nullable()->buildingNumber(),
            'block' => $this->faker->nullable()->numerify('##'),
            'flat' => $this->faker->nullable()->numerify('##'),
            'floor' => $this->faker->nullable()->numerify('##'),
            'porch' => $this->faker->nullable()->numerify('##'),
            'intercom' => $this->faker->nullable()->numerify('##-##'),
            'geo_lat' => (string)$this->faker->latitude(),
            'geo_lon' => (string)$this->faker->longitude(),
        ];
    }

    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
