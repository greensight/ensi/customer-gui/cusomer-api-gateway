<?php

namespace App\Domain\Orders\Tests\Baskets\Factories;

use Ensi\BasketsClient\Dto\Discount;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CalculatedDiscountFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'value_type' => $this->faker->randomNumber(),
            'value' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): Discount
    {
        return new Discount($this->makeArray($extra));
    }
}
