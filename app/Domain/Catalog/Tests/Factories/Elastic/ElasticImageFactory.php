<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\ElasticImage;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ElasticImageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->company,
            'sort' => $this->faker->randomNumber(),
            'url' => $this->faker->imageUrl,
        ];
    }

    public function make(array $extra = []): ElasticImage
    {
        return new ElasticImage($this->makeArray($extra));
    }
}
