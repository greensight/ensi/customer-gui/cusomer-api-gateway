<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\AttributeValue;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class ElasticAttributeValueFactory extends BaseApiFactory
{
    protected mixed $value;

    protected function definition(): array
    {
        return [
            'name' => $this->faker->nullable()->sentence,
            'value' => $this->value ?? $this->faker->word,
        ];
    }

    public function make(array $extra = []): AttributeValue
    {
        return new AttributeValue($this->makeArray($extra));
    }

    public function valueByType(string $type): self
    {
        $value = match ($type) {
            PropertyTypeEnum::BOOLEAN => $this->faker->boolean,
            PropertyTypeEnum::INTEGER => $this->faker->numberBetween(1),
            PropertyTypeEnum::DOUBLE => $this->faker->randomFloat(min: 0.1),
            default => $this->faker->word
        };

        return $this->immutableSet('value', $value);
    }
}
