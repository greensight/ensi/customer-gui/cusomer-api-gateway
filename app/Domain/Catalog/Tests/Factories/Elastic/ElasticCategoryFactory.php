<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\ElasticCategory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ElasticCategoryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'parent_id' => $this->faker->nullable()->modelId(),
        ];
    }

    public function make(array $extra = []): ElasticCategory
    {
        return new ElasticCategory($this->makeArray($extra));
    }
}
