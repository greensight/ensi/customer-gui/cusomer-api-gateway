<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\CatalogCacheClient\Dto\ElasticBrand;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;

class ElasticBrandFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $isExternal = $this->faker->boolean();

        return [
            'id' => $this->faker->modelId(),
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'description' => $this->faker->nullable()->sentence,
            'logo_file' => $this->when(!$isExternal, fn () => new File(EnsiFileFactory::new()->make())),
            'logo_url' => $this->when($isExternal, $this->faker->url()),
        ];
    }

    public function make(array $extra = []): ElasticBrand
    {
        return new ElasticBrand($this->makeArray($extra));
    }
}
