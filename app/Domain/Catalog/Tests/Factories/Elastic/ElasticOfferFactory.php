<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\ElasticBrand;
use Ensi\CatalogCacheClient\Dto\ElasticDiscount;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\ElasticOfferResponse;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LaravelTestFactories\FactoryMissingValue;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Illuminate\Support\Collection;

class ElasticOfferFactory extends BaseApiFactory
{
    public ?ElasticBrand $brand = null;
    public ?ElasticDiscount $discount = null;
    public ?Collection $categories = null;
    public ?Collection $attributes = null;
    public ?Collection $images = null;
    public ?Collection $nameplates = null;
    public ?Collection $gluing = null;

    protected function definition(): array
    {
        $hasGluing = $this->faker->boolean;

        $type = $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues());
        $isWeigh = $type === ProductTypeEnum::WEIGHT;

        return [
            'id' => $this->faker->modelId(),

            'product_id' => $this->faker->modelId(),
            'allow_publish' => $this->faker->boolean(),

            'main_image_file' => new File(EnsiFile::factory()->make()),

            'category_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), min: 1),
            'brand_id' => $this->faker->nullable()->modelId(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->text(50),
            'type' => $type,
            'vendor_code' => $this->faker->nullable()->numerify('######'),
            'barcode' => $this->faker->nullable()->ean13(),

            'weight' => $this->faker->nullable()->randomFloat(4),
            'weight_gross' => $this->faker->nullable()->randomFloat(4),
            'length' => $this->faker->nullable()->randomNumber(),
            'width' => $this->faker->nullable()->randomNumber(),
            'height' => $this->faker->nullable()->randomNumber(),
            'is_adult' => $this->faker->boolean(),
            'uom' => $this->faker->nullable($isWeigh)->randomElement(ProductUomEnum::getAllowableEnumValues()),
            'tariffing_volume' => $this->faker->nullable($isWeigh)->randomElement(ProductTariffingVolumeEnum::getAllowableEnumValues()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),

            'price' => $this->faker->nullable()->randomNumber(),

            'gluing_name' => $hasGluing ? $this->faker->sentence() : null,
            'gluing_is_main' => $hasGluing ? $this->faker->boolean : null,
            'gluing_is_active' => $hasGluing ? $this->faker->boolean : null,

            'brand' => $this->notNull($this->brand),
            'discount' => $this->notNull($this->discount),
            'categories' => $this->executeNested($this->categories, new FactoryMissingValue()),
            'nameplates' => $this->executeNested($this->nameplates, new FactoryMissingValue()),
            'attributes' => $this->executeNested($this->attributes, new FactoryMissingValue()),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
            'gluing' => $this->executeNested($this->gluing, new FactoryMissingValue()),
        ];
    }

    public function make(array $extra = []): ElasticOffer
    {
        return new ElasticOffer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ElasticOfferResponse
    {
        return new ElasticOfferResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchElasticOffersResponse
    {
        return $this->generateResponseSearch(SearchElasticOffersResponse::class, $extras, $count, $pagination);
    }

    public function withBrand(): self
    {
        return $this->immutableSet('brand', ElasticBrandFactory::new()->make());
    }

    public function withCategories(int $count = 1): self
    {
        $categories = Collection::times($count, fn () => ElasticCategoryFactory::new());

        return $this->immutableSet('categories', $categories);
    }

    public function withAttributes(int $count = 1): self
    {
        $attributes = Collection::times($count, fn () => ElasticAttributeFactory::new());

        return $this->immutableSet('attributes', $attributes);
    }

    public function withImages(int $count = 1): self
    {
        $images = Collection::times($count, fn () => ElasticImageFactory::new());

        return $this->immutableSet('images', $images);
    }

    public function withNameplates(int $count = 1): self
    {
        $nameplates = Collection::times($count, fn () => ElasticNameplateFactory::new());

        return $this->immutableSet('nameplates', $nameplates);
    }

    public function withGluing(int $count = 1): self
    {
        $gluing = Collection::times($count, fn () => ElasticGluingFactory::new());

        return $this->immutableSet('gluing', $gluing);
    }

    public function withDiscount(): self
    {
        return $this->immutableSet('discount', ElasticDiscountFactory::new()->make());
    }
}
