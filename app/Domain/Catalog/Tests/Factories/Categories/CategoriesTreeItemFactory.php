<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\CategoriesTreeItem;
use Ensi\PimClient\Dto\CategoriesTreeResponse;

class CategoriesTreeItemFactory extends BaseApiFactory
{
    public int $childrenCount = 0;
    public int $depth = 0;

    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(2),
            'code' => $this->faker->slug(2),
            'children' => $this->when($this->childrenCount > 0 && $this->depth > 0, fn () => $this->generateChildren()),
        ];
    }

    public function make(array $extra = []): CategoriesTreeItem
    {
        return new CategoriesTreeItem($this->makeArray($extra));
    }

    public function makeResponse(int $count = 1, array $extra = []): CategoriesTreeResponse
    {
        return new CategoriesTreeResponse(['data' => $this->makeSeveral($count, $extra)->all()]);
    }

    public function withChildren(int $count): self
    {
        return $this->immutableSet('childrenCount', $count);
    }

    public function depth(int $value): self
    {
        return $this->immutableSet('depth', $value);
    }

    private function generateChildren(): array
    {
        $childFactory = self::new()->withChildren($this->childrenCount);

        if ($this->depth > 1) {
            $childFactory = $childFactory->depth($this->depth - 1);
        }

        return $childFactory->makeSeveral($this->childrenCount)->toArray();
    }
}
