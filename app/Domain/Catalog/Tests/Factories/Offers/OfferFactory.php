<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;

class OfferFactory extends BaseApiFactory
{
    protected array $stocks = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
            'allow_publish' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'active_comment' => $this->faker->nullable()->word(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->stocks) {
            $definition['stocks'] = $this->stocks;
        }

        return $definition;
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OfferResponse
    {
        return new OfferResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extras, $count, $pagination);
    }

    public function withStock(?Stock $stock = null): self
    {
        $this->stocks[] = $stock ?: StockFactory::new()->make();

        return $this;
    }
}
