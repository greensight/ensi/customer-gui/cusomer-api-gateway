<?php

namespace App\Domain\Catalog\Actions\Reviews;

use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\Dto\CreateReviewRequest;
use Ensi\ReviewsClient\Dto\Review;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;

class CreateReviewAction
{
    public function __construct(protected readonly ReviewsApi $reviewsApi)
    {
    }

    public function execute(array $data): Review
    {
        $request = new CreateReviewRequest($data);
        $request->setCustomerId(user()->customerId);
        $request->setStatusId(ReviewStatusEnum::NEW);

        return $this->reviewsApi->createReview($request)->getData();
    }
}
