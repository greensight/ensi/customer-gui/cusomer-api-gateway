<?php

namespace App\Domain\Catalog\Data\Reviews;

use Ensi\CustomersClient\Dto\Customer;
use Ensi\ReviewsClient\Dto\Review;

class ReviewData
{
    public function __construct(
        public readonly Review $review,
        public readonly ?Customer $customer = null,
    ) {
    }
}
