<?php

namespace App\Domain\Units\Actions\Bu;

use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\CreateSellerRequest;
use Ensi\BuClient\Dto\Seller;

class CreateSellerAction
{
    public function __construct(protected readonly SellersApi $sellersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Seller
    {
        $seller = new CreateSellerRequest($fields);

        return $this->sellersApi->createSeller($seller)->getData();
    }
}
